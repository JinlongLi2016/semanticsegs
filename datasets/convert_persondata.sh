
CURRENT_DIR=$(pwd)
WORK_DIR="./pascal_voc_seg"

PASCAL_ROOT="./PersonData"
IMAGE_FOLDER="${PASCAL_ROOT}/ds/img"
LIST_FOLDER="${PASCAL_ROOT}/splits"
SEMANTIC_SEG_FOLDER="${PASCAL_ROOT}/segs"
OUTPUT_DIR="${PASCAL_ROOT}/tfrecord"
python ./build_voc2012_data.py \
  --image_folder="${IMAGE_FOLDER}" \
  --semantic_segmentation_folder="${SEMANTIC_SEG_FOLDER}" \
  --list_folder="${LIST_FOLDER}" \
  --image_format="png" \
  --output_dir="${OUTPUT_DIR}"