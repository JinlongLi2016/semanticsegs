#!/usr/bin/env bash

# Script to unzip and preprocess the bottle dataset.
#
# Usage:
#   bash ./download_and_convert_bottle.sh
#

# The folder structure is assumed to be:
#  + datasets
#     - build_data.py
#     - build_voc2012_data.py
#     - download_and_convert_voc2012.sh
#     - remove_gt_colormap.py
#     + pascal_voc_seg
#       + VOCdevkit
#         + VOC2012
#           + JPEGImages
#           + SegmentationClass
#

# Exit immediately if a command exits with a non-zero status.
set -e
CURRENT_DIR=$(pwd)
WORK_DIR="./dummy_bottle"
mkdir -p "${WORK_DIR}"
cd "${WORK_DIR}"

# Helper function to unpack bottle dataset.
download_and_uncompress() {
    local BASE_URL=${1}
    local FILENAME=${2}

    if [ ! -f "${FILENAME}" ]; then
        echo "need to move ${FILENAME} into ${WORK_DIR}"
        exit 1
    fi
    echo "Uncompressing ${FILENAME}"
    tar -xf "${FILENAME}"
}

# put bottle dataset under work_dir manually
BASE_URL="${WORK_DIR}"
FILENAME="bottle1026.tar"

download_and_uncompress "${BASE_URL}" "${FILENAME}"

cd "${CURRENT_DIR}"

# Root path for bottle dataset
BOTTLE_ROOT="${WORK_DIR}/newdata"

# generate masks
SEG_DESC_FOLDER="${BOTTLE_ROOT}/"
SEMANTIC_SEG_FOLDER="${BOTTLE_ROOT}/SegmentationClassRaw"

# Build TFRecords of the dataset
# First, create output directory for storing TFRecords
OUTPUT_DIR="${WORK_DIR}/tfrecord"
mkdir -p "${OUTPUT_DIR}"

IMAGE_FOLDER="${BOTTLE_ROOT}/"

echo "Converting bottle 2018_10_26 dataset..."
python ./build_bottle_data.py --dataset_dir=./dummy_bottle/newdata \
--output_tf_dir=./dummy_bottle/tfrecord \
--output_seg_dir=./dummy_bottle/newdata/SegmentationClassRaw