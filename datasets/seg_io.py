# tfrecords io
import sys
import os
sys.path.append(os.getcwd())
import math
import os.path
import sys
# import build_data
import tensorflow as tf
import json
import numpy as np 
import skimage
import utils
import matplotlib.image as mpimg
import matplotlib.pyplot as plt 
import collections
import six

slim = tf.contrib.slim

dataset = slim.dataset

tfexample_decoder = slim.tfexample_decoder


class PersonDataset(utils.Dataset):
    def load_persons(self, root_dir):
        self.add_class("SuperviselyPerson", 1, 'person')
        anns_path = os.path.join(root_dir, "ann")
        for file in os.listdir(anns_path):
            if file.endswith(".json"):
                with open(os.path.join(anns_path, file), mode='r') as annf:
                    ann_info = json.load(annf)
                #
                # polys_points = ann_info['objects']['points']['exterior']
                polys_points = [obj['points']['exterior'] for obj in ann_info['objects']]
                # 如果有多个物体如何处理？
                # polys_points 是一个列表，每个元素代表了一个目标的外轮廓

                fname, ext = os.path.splitext(file)
                img_path = os.path.join(root_dir, 'img', fname+'.png')

                img = mpimg.imread(img_path)
                s = img.shape
                w, h = s[1], s[0]
                print(fname ,img_path)
                self.add_image('SuperviselyPerson',
                                image_id=img_path,
                                path=img_path,
                                width=w, height=h,
                                polys_points=polys_points)


    def load_mask(self, image_id):
        """Generate instance masks for an image.
       Returns:
        masks: A bool array of shape [height, width, instance count] with
            one mask per instance.
        class_ids: a 1D array of class IDs of the instance masks.
        """
        # If not a balloon dataset image, delegate to parent class.
        info = self.image_info[image_id]

        # if info["source"] != "balloon":
        #     return super(self.__class__, self).load_mask(image_id)

        # Convert polygons to a bitmap mask of shape
        # [height, width, instance_count]
        mask = np.zeros([info["height"], info["width"], 1],
                        dtype=np.uint8)
        print('mask.shape:', mask.shape)

        polys_points =  info['polys_points']
        for poly_points in polys_points:
            # if isinstance(poly_points[0], float):
            #     print(poly_points)
            # print(poly_points)
            xs = [p[0] for p in poly_points]
            ys = [p[1] for p in poly_points]

            if len(xs)>=1:
                rr, cc = skimage.draw.polygon(ys, xs)
                mask[rr, cc, 0] = 1

        return mask.astype(np.bool), np.ones([mask.shape[-1]], dtype=np.int32)
        # 在supervised data上面返回第二个类别没有？（因为都是


    def image_reference(self, image_id):
        info = self.image_info[image_id]

        if info["source"] == "SuperviselyPerson":
            return info['path']
        else:
            super(self.__class__, self).image_reference(image_id)

    def load_wh(self, image_id):
        info = self.image_info[image_id]
        return info['width'], info['height']

    def show(self, image_id):
        mask, _ = self.load_mask(image_id)
        # print(mask.max(), max.min())
        img = self.load_image(image_id)
        img[mask[...,0] == True] = [61, 145, 64]
        plt.imshow(img)
        plt.show()


    def inspect_data(self):
        """这个函数用于检查图片大小 max() min(), mask的形状 mask大小 dtype ？"""
        pass

    def save_as_tfrecords(self, name = "train_test.tfrecords"):
        nb_images = self.num_images
        with tf.python_io.TFRecordWriter(name) as writer:
            for img_id in range(nb_images): # self.image_ids?
                img = self.load_image(img_id)
                mask, _ = self.load_mask(img_id)
                w, h = self.load_wh(img_id)
                feature = {'mask':_bytes_feature(mask.tostring()),
                            'image':_bytes_feature(img.tostring()),
                            'width':_int64_feature(w),
                            'height':_int64_feature(h)}
                example = tf.train.Example(features=tf.train.Features(feature=feature))
                writer.write(example.SerializeToString())

    def save_as_deeplab_records(self, name="deeplab_data.tfrecords"):
        _NUM_SHARDS = 5
        nb_images = self.num_images
        num_per_shard = int(math.ceil(nb_images / _NUM_SHARDS))
        
        image_reader = build_data.ImageReader('jpeg', channels=3)
        label_reader = build_data.ImageReader('png', channels=1)

        image_ids = self.image_ids 
        dataset = "train"
        for shard_id in range(_NUM_SHARDS):
            output_filename = os.path.join(
                os.getcwd(), 
                '%s-%05d-of-%05d.tfrecord' % (dataset, shard_id, _NUM_SHARDS))
            with tf.python_io.TFRecordWriter(output_filename) as writer:
                start_idx = shard_id * num_per_shard
                end_idx = min((shard_id + 1) * num_per_shard, nb_images)
                for i in range(start_idx, end_idx):
                    sys.stdout.write('\r>> Converting image %d/%d shard %d' % (
                    i + 1, nb_images, shard_id))
                    sys.stdout.flush()

                    image_filename = self.image_reference(i)
                    image_data = tf.gfile.FastGFile(image_filename, 'rb').read()
                    width, height = self.load_wh(i)

                    mask, _ = self.load_mask(i)
                    print(height, width, mask.shape)
                    if  mask.shape[0] != height and mask.shape[1] != width:
                        raise RuntimeError('shape mismatched.')

                    seg_data = mask.tostring()
                    example = build_data.image_seg_to_tfexample(image_data, image_filename, "png", height,
                        width, seg_data, "png")

                    writer.write(example.SerializeToString())
            sys.stdout.write('\n')
            sys.stdout.flush()

    def load_deeplab_tfrecords(self, dataset_dir, split_name, name=None):
        file_pattern = '%s-*'
        file_pattern = os.path.join(dataset_dir, file_pattern%split_name)

        keys_to_features = {
            'image/encoded': tf.FixedLenFeature(
                (), tf.string, default_value=''),
            'image/filename': tf.FixedLenFeature(
                (), tf.string, default_value=''),
            'image/format': tf.FixedLenFeature(
                (), tf.string, default_value=''),
            'image/height': tf.FixedLenFeature(
                (), tf.int64, default_value=0),
            'image/width': tf.FixedLenFeature(
                (), tf.int64, default_value=0),
            'image/segmentation/class/encoded': tf.FixedLenFeature(
                (), tf.string, default_value=''),
            'image/segmentation/class/format': tf.FixedLenFeature(
                (), tf.string, default_value='png')
        }
        items_to_handlers = {
            'image': tfexample_decoder.Image(
                image_key='image/encoded',
                format_key='image/format',
                channels=3),
            'image_name': tfexample_decoder.Tensor('image/filename'),
            'height': tfexample_decoder.Tensor('image/height'),
            'width': tfexample_decoder.Tensor('image/width'),
            'labels_class': tfexample_decoder.Image(
                image_key='image/segmentation/class/encoded',
                format_key='image/segmentation/class/format',
                channels=1),
        }

        decoder = tfexample_decoder.TFExampleDecoder(
            keys_to_features, items_to_handlers)

        ds = dataset.Dataset(
            data_sources=file_pattern,
            reader=tf.TFRecordReader,
            decoder=decoder,
            num_samples=10,
            items_to_descriptions={},
            num_classes=2,
            name="SuperviselyPerson",
            multi_label=True)
        return ds

    def load_tfrecords(self, name="train_test.tfrecords"):
        # reader = tf.TFRecordReader()
        # fname_queue = tf.train.string_input_producer([name])
        # _, serialized_example = reader.read(fname_queue)
        # feature_set = {'mask':tf.FixedLenFeature([], tf.string),
        #                 'image':tf.FixedLenFeature([], tf.string)}
        # features = tf.parse_single_example(serialized_example,
        #                                     features=feature_set)

        # img, mask = features['image'], features['mask']
        # img = tf.decode_raw(img, tf.int32)
        # mask = tf.decode_raw(mask, tf.int32)

        # sess = tf.Session()
        # with sess.as_default():
        #     coord = tf.train.Coordinator()
        #     threads = tf.train.start_queue_runners(coord = coord)
        #     img, mask = sess.run([img, mask])
        #     self.ins(img)
        #     self.ins(mask)

        reconstructed_images = []
        record_iterator = tf.python_io.tf_record_iterator(path=name)
        for string_record in record_iterator:
            example = tf.train.Example()
            example.ParseFromString(string_record)

            height = int(example.features.feature['height']
                .int64_list
                .value[0])
            width = int(example.features.feature['width']
                .int64_list
                .value[0])

            mask_string = (example.features.feature['mask']
                .bytes_list
                .value[0])
            img_string = (example.features.feature['image']
                .bytes_list
                .value[0])

            img_1d = np.fromstring(img_string, dtype=np.uint8)
            reconstructed_img = img_1d.reshape((height, width, -1))

            ann_1d = np.fromstring(mask_string, dtype=np.uint8)
            reconstructed_annotation = ann_1d.reshape((height, width))
            reconstructed_images.append([reconstructed_img, reconstructed_annotation])
        plt.imshow(reconstructed_images[0][0])

        plt.show()
        plt.imshow(reconstructed_images[1][1])
        plt.show() 
        print(len(reconstructed_images))

    def ins(self, arr, name=None):
        print(arr.max(), arr.min())

def _int64_list_feature(values):
  """Returns a TF-Feature of int64_list.

  Args:
    values: A scalar or list of values.

  Returns:
    A TF-Feature.
  """
  if not isinstance(values, collections.Iterable):
    values = [values]

  return tf.train.Feature(int64_list=tf.train.Int64List(value=values))


def _bytes_list_feature(values):
  """Returns a TF-Feature of bytes.

  Args:
    values: A string.

  Returns:
    A TF-Feature.
  """
  def norm2bytes(value):
    return value.encode() if isinstance(value, str) and six.PY3 else value

  return tf.train.Feature(
      bytes_list=tf.train.BytesList(value=[norm2bytes(values)]))
def _int64_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))
def _bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

if __name__ == '__main__':
    ds = PersonDataset()
    ds.load_persons("../datasets/ds10")
    ds.prepare()
    # mask, cid = ds.load_mask(10)
    # print(mask.shape, cid.shape)
    # ds.show(10)
    # ds.save_as_tfrecords()
    ds.save_as_deeplab_records()
    ds.load_tfrecords()
    ds.load_deeplab_tfrecords("./", "train")
