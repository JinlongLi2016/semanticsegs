import tensorflow as tf

saved_model_dir='./datasets/dummy_bottle/exp/train_mobilenetv2_run3/train'
converter = tf.contrib.lite.TFLiteConverter.from_saved_model(saved_model_dir)
tflite_model = converter.convert()
open("converted_model.tflite", "wb").write(tflite_model)